import axios from 'axios';

const product = {
  namespaced: true,
  actions: {
    async ListProduct(_, options) {
      let response = await axios({
        method: 'get',
        url: `http://localhost:3000/api/product/find`,
        params: options
        
      });
      return response.data
    },
    async CreateProduct(_, product) {
      let response = await axios({
        method: 'post',
        url: `http://localhost:3000/api/product/create`,
        data: product
      });
      return response.data
    },
    async UpdateProduct(_, product) {
      let response = await axios({
        method: 'post',
        url: `http://localhost:3000/api/product/update`,
        data: product
      });
      return response.data
    },
    async GetOneProduct(_, id) {
      let response = await axios({
        method: 'get',
        url: `http://localhost:3000/api/product?id=${id}`,
      });
      return response.data
    },
    async DeleteProduct(_, id) {
      let response = await axios({
        method: 'post',
        url: `http://localhost:3000/api/product/delete`,
        data: {
          id: id
        }
      });
      return response.data
    },
    async GetConfig(_, options) {
      let response = await axios({
        method: 'get',
        url: `http://localhost:3000/api/config/find`,
        params: {
           ...options,
          cauHinh: "danhmuc"
        }
      });
      return response.data
    },
    async ChangeStatus(_, options) {
      let response = await axios({
        method: 'post',
        url: `http://localhost:3000/api/product/change-status`,
        data: options
      });
      return response.data
    },
  }
}

export default product