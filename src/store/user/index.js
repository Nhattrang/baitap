import axios from 'axios';

const user = {
  namespaced: true,
  actions: {
        async ListUser(_, options) {
            let response = await axios({
            method: 'get',
            url: `http://localhost:3000/api/user/find`,
            params: {
                ...options,
                chucVu:0
            }
            });
            return response.data
        },
        async UpdateUser(_, user) {
            let response = await axios({
              method: 'post',
              url: `http://localhost:3000/api/user/update`,
              data: user
            });
            return response.data
        },
        async GetOneUser(_, id) {
            let response = await axios({
                method: 'get',
                url: `http://localhost:3000/api/user?id=${id}`,
            });
            return response.data
        },
        async CreateUser(_, user) {
            let response = await axios({
              method: 'post',
              url: 'http://localhost:3000/api/user/create',
              data: user,
            });
            return response
        },
    }
}
export default user