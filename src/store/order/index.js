import axios from 'axios';
const order = {
    namespaced: true,
    actions: {
        async ListOrder(_, options) {
            let response = await axios({
                method: 'get',
                url: `http://localhost:3000/api/order/find`,
                params: options

            });
            return response.data
        },
        async GetOneOrder(_, id) {
            let response = await axios({
                method: 'get',
                url: `http://localhost:3000/api/order?id=${id}`,
            });
            return response.data
        },
        async CreateOrder(_, order) {
            let response = await axios({
                method: 'post',
                url: `http://localhost:3000/api/order/create`,
                data: order
            });
            return response.data
        },
        async Change_Status(_, options) {
            let response = await axios({
              method: 'post',
              url: `http://localhost:3000/api/order/change-status`,
              data: options
            });
            return response.data
        },
        async ListProduct(_, options) {
            let response = await axios({
              method: 'get',
              url: `http://localhost:3000/api/product/find`,
              params: options
              
            });
            return response.data
        },
        async UpdateOrder(_, order) {
            let response = await axios({
              method: 'post',
              url: `http://localhost:3000/api/order/update`,
              data: order
            });
            return response.data
        },
        async DeleteOrder(_, id) {
            let response = await axios({
              method: 'post',
              url: `http://localhost:3000/api/order/delete`,
              data: {
                id: id
              }
            });
            return response.data
          },
    }
}
export default order