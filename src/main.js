import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vuex from 'vuex'
import store from './store/index'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VueApexCharts from 'vue-apexcharts'

Vue.config.productionTip = false
Vue.use(Vuex);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin)
Vue.use(store)
Vue.use(VueApexCharts)
Vue.component('apexchart', VueApexCharts)



// Install the store instance as a plugin

const app = new Vue({
  router:router,
  store: store,
  render: h => h(App),
  components: {
    apexchart: VueApexCharts,
  },
})

app.$mount('#app')